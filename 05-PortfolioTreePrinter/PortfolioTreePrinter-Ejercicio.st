!classDefinition: #AccountTransaction category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountTransaction methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:20:37'!
affectBalance: currentBalance  

	self subclassResponsibility ! !

!AccountTransaction methodsFor: 'evaluating' stamp: 'HernanWilkinson 7/14/2011 06:48'!
value

	self subclassResponsibility ! !


!AccountTransaction methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:22:36'!
accept: aVisitor

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'HAW 10/18/2018 21:06:37'!
for: anAmount

	self subclassResponsibility ! !


!AccountTransaction class methodsFor: 'registering' stamp: 'HAW 10/18/2018 19:48:18'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #CertificateOfDeposit category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction subclass: #CertificateOfDeposit
	instanceVariableNames: 'value numberOfDays interestRate'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!CertificateOfDeposit methodsFor: 'evaluating' stamp: 'DEG 10/21/2018 17:17:12'!
affectBalance: currentBalance  

	^currentBalance - self value! !

!CertificateOfDeposit methodsFor: 'evaluating' stamp: 'DEG 10/21/2018 17:38:09'!
earnings 

	^ value*interestRate/360*numberOfDays ! !


!CertificateOfDeposit methodsFor: 'initialization' stamp: 'DEG 10/21/2018 17:15:26'!
initializeOf: anAmount during: aNumberOfDays at: anInterestRate

	value := anAmount. 
	numberOfDays := aNumberOfDays.
	interestRate := anInterestRate.! !


!CertificateOfDeposit methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:23:27'!
accept: aVisitor

	aVisitor visitCertificateOfDeposit: self! !


!CertificateOfDeposit methodsFor: 'accessing' stamp: 'DEG 10/21/2018 17:45:40'!
interestRate

	^ interestRate! !

!CertificateOfDeposit methodsFor: 'accessing' stamp: 'DEG 10/21/2018 17:45:35'!
numberOfDays

	^ numberOfDays! !

!CertificateOfDeposit methodsFor: 'accessing' stamp: 'DEG 10/21/2018 17:21:04'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CertificateOfDeposit class' category: #'PortfolioTreePrinter-Ejercicio'!
CertificateOfDeposit class
	instanceVariableNames: ''!

!CertificateOfDeposit class methodsFor: 'instance creation' stamp: 'DEG 10/21/2018 17:14:03'!
of: anAmount during: aNumberOfDays at: anInterestRate

	^self new initializeOf: anAmount during: aNumberOfDays at: anInterestRate! !


!CertificateOfDeposit class methodsFor: 'registering' stamp: 'DEG 10/21/2018 17:14:16'!
register: anAmount during: aNumberOfDays at: anInterestRate on: anAccount

	| certificateOfDeposit |
	
	certificateOfDeposit := self of: anAmount during: aNumberOfDays at: anInterestRate.
	anAccount  register: certificateOfDeposit.
	
	^ certificateOfDeposit ! !


!classDefinition: #Deposit category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'HAW 10/18/2018 20:37:28'!
affectBalance: currentBalance  

	^currentBalance + self value! !

!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !


!Deposit methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:23:53'!
accept: aVisitor

	aVisitor visitDeposit: self! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: #'PortfolioTreePrinter-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #TransferLeg category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction subclass: #TransferLeg
	instanceVariableNames: 'transfer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!TransferLeg methodsFor: 'transfer' stamp: 'HAW 10/18/2018 21:05:34'!
transfer

	^transfer ! !


!TransferLeg methodsFor: 'initialization' stamp: 'HAW 10/18/2018 21:07:15'!
initializeRelatedTo: aTransfer 
	
	transfer := aTransfer ! !


!TransferLeg methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:35:56'!
value

	^transfer value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TransferLeg class' category: #'PortfolioTreePrinter-Ejercicio'!
TransferLeg class
	instanceVariableNames: ''!

!TransferLeg class methodsFor: 'instance creation' stamp: 'HAW 10/18/2018 21:07:07'!
relatedTo: aTransfer 
	
	^self new initializeRelatedTo: aTransfer ! !


!classDefinition: #TransferDeposit category: #'PortfolioTreePrinter-Ejercicio'!
TransferLeg subclass: #TransferDeposit
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!TransferDeposit methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:34:58'!
affectBalance: currentBalance

	^currentBalance + self value! !


!TransferDeposit methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:24:14'!
accept: aVisitor

	aVisitor visitTransferDeposit: self! !


!classDefinition: #TransferWithdraw category: #'PortfolioTreePrinter-Ejercicio'!
TransferLeg subclass: #TransferWithdraw
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!TransferWithdraw methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 20:34:40'!
affectBalance: currentBalance

	^currentBalance - self value! !


!TransferWithdraw methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:25:08'!
accept: aVisitor

	aVisitor visitTransferWithdraw: self! !


!classDefinition: #Withdraw category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'HAW 10/18/2018 20:36:35'!
affectBalance: currentBalance  

	^currentBalance - self value! !

!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/14/2011 05:54'!
value

	^ value ! !


!Withdraw methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:24:53'!
accept: aVisitor

	aVisitor visitWithdraw: self! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: #'PortfolioTreePrinter-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #AccountTransactionsVisitor category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #AccountTransactionsVisitor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountTransactionsVisitor methodsFor: 'initialization' stamp: 'GB 10/22/2018 20:44:41'!
initializeOf: anAccount

	self subclassResponsibility ! !


!AccountTransactionsVisitor methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:12:02'!
visitCertificateOfDeposit: aCertificateOfDeposit

	self subclassResponsibility ! !

!AccountTransactionsVisitor methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:02:23'!
visitDeposit: aDeposit

	self subclassResponsibility ! !

!AccountTransactionsVisitor methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:02:36'!
visitTransferDeposit: aTransferDeposit

	self subclassResponsibility ! !

!AccountTransactionsVisitor methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:03:08'!
visitTransferWithdraw: aTransferWithdraw

	self subclassResponsibility ! !

!AccountTransactionsVisitor methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:03:17'!
visitWithdraw: aWithdraw

	self subclassResponsibility ! !


!AccountTransactionsVisitor methodsFor: 'evaluating' stamp: 'GB 10/22/2018 18:59:18'!
value

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransactionsVisitor class' category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionsVisitor class
	instanceVariableNames: ''!

!AccountTransactionsVisitor class methodsFor: 'instance creation' stamp: 'GB 10/22/2018 18:51:53'!
of: anAccount

	^ self new initializeOf: anAccount ! !


!classDefinition: #AccountInvestmentEarnings category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionsVisitor subclass: #AccountInvestmentEarnings
	instanceVariableNames: 'investmentEarnings account'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountInvestmentEarnings methodsFor: 'evaluating' stamp: 'GB 10/22/2018 19:10:40'!
value

	investmentEarnings := 0.
	account visitTransactionsWith: self.
	
	^ investmentEarnings! !


!AccountInvestmentEarnings methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:14:14'!
visitCertificateOfDeposit: aCertificateOfDeposit

	investmentEarnings := investmentEarnings + aCertificateOfDeposit earnings ! !

!AccountInvestmentEarnings methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:09:32'!
visitDeposit: aDeposit! !

!AccountInvestmentEarnings methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:13:05'!
visitTransferDeposit: aTransferDeposit
! !

!AccountInvestmentEarnings methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:13:09'!
visitTransferWithdraw: aTransferWithdraw
! !

!AccountInvestmentEarnings methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:09:32'!
visitWithdraw: aWithdraw! !


!AccountInvestmentEarnings methodsFor: 'initialization' stamp: 'GB 10/22/2018 20:45:26'!
initializeOf: anAccount

	account := anAccount ! !


!classDefinition: #AccountInvestmentNet category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionsVisitor subclass: #AccountInvestmentNet
	instanceVariableNames: 'investmentNet account'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountInvestmentNet methodsFor: 'evaluating' stamp: 'GB 10/22/2018 19:15:03'!
value

	investmentNet := 0.
	account visitTransactionsWith: self.
	
	^ investmentNet! !


!AccountInvestmentNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:15:54'!
visitCertificateOfDeposit: aCertificateOfDeposit

	investmentNet := investmentNet + aCertificateOfDeposit value! !

!AccountInvestmentNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:14:45'!
visitDeposit: aDeposit! !

!AccountInvestmentNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:14:45'!
visitTransferDeposit: aTransferDeposit
! !

!AccountInvestmentNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:14:45'!
visitTransferWithdraw: aTransferWithdraw
! !

!AccountInvestmentNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:14:45'!
visitWithdraw: aWithdraw! !


!AccountInvestmentNet methodsFor: 'initialization' stamp: 'GB 10/22/2018 20:46:02'!
initializeOf: anAccount

	account := anAccount ! !


!classDefinition: #AccountSummaryLines category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionsVisitor subclass: #AccountSummaryLines
	instanceVariableNames: 'lines account'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountSummaryLines methodsFor: 'evaluating' stamp: 'GB 10/22/2018 19:17:05'!
value

	lines := OrderedCollection new.
	account visitTransactionsWith: self.
	
	^ lines! !


!AccountSummaryLines methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:18:46'!
visitCertificateOfDeposit: aCertificateOfDeposit

	lines add: 'Plazo fijo por ', 
	aCertificateOfDeposit value printString , 
	' durante ', 
	aCertificateOfDeposit numberOfDays printString, 
	' dias a una tna de ', 
	(aCertificateOfDeposit interestRate * 100) printString, 
	'%'! !

!AccountSummaryLines methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:19:22'!
visitDeposit: aDeposit

	lines add: 'Deposito por ', aDeposit value printString! !

!AccountSummaryLines methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:19:45'!
visitTransferDeposit: aTransferDeposit

	lines add: 'Transferencia por ', aTransferDeposit value printString! !

!AccountSummaryLines methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:20:01'!
visitTransferWithdraw: aTransferWithdraw

	lines add: 'Transferencia por ', aTransferWithdraw value negated printString! !

!AccountSummaryLines methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:20:19'!
visitWithdraw: aWithdraw

	lines add: 'Extraccion por ', aWithdraw value printString! !


!AccountSummaryLines methodsFor: 'initialization' stamp: 'GB 10/22/2018 20:46:09'!
initializeOf: anAccount

	account := anAccount ! !


!classDefinition: #AccountTransferNet category: #'PortfolioTreePrinter-Ejercicio'!
AccountTransactionsVisitor subclass: #AccountTransferNet
	instanceVariableNames: 'transferNet account'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!AccountTransferNet methodsFor: 'evaluating' stamp: 'GB 10/22/2018 19:05:19'!
value

	transferNet := 0.
	account visitTransactionsWith: self.
	
	^ transferNet! !


!AccountTransferNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:12:18'!
visitCertificateOfDeposit: aCertificateOfDeposit
! !

!AccountTransferNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:06:05'!
visitDeposit: aDeposit! !

!AccountTransferNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:07:15'!
visitTransferDeposit: aTransferDeposit

	transferNet  := transferNet + aTransferDeposit value! !

!AccountTransferNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:07:58'!
visitTransferWithdraw: aTransferWithdraw

	transferNet  := transferNet - aTransferWithdraw value! !

!AccountTransferNet methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:08:21'!
visitWithdraw: aWithdraw! !


!AccountTransferNet methodsFor: 'initialization' stamp: 'GB 10/22/2018 20:46:14'!
initializeOf: anAccount

	account := anAccount ! !


!classDefinition: #PortfolioTreePrinter category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #PortfolioTreePrinter
	instanceVariableNames: 'portfolio accountNames lines level'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!PortfolioTreePrinter methodsFor: 'initialization' stamp: 'GB 10/22/2018 19:40:23'!
initializeOf: aPortfolio namingAccountWith: aDictionary

	portfolio := aPortfolio.
	accountNames := aDictionary ! !


!PortfolioTreePrinter methodsFor: 'visiting' stamp: 'GB 10/22/2018 20:21:05'!
visitPortfolio: aPortfolio
	
	lines add: ( ( ( String new: level ) atAllPut: $ ) , ( accountNames at: aPortfolio ) ).
	level := level + 1.
	aPortfolio visitAccountsWith: self.
	level := level - 1! !

!PortfolioTreePrinter methodsFor: 'visiting' stamp: 'GB 10/22/2018 20:21:30'!
visitReceptiveAccount: aReceptiveAccount

	lines add:  ( ( ( String new: level ) atAllPut: $ ), (accountNames at: aReceptiveAccount ) )! !


!PortfolioTreePrinter methodsFor: 'evaluating' stamp: 'GB 10/22/2018 20:49:08'!
reverseValue

	^ self value reverse! !

!PortfolioTreePrinter methodsFor: 'evaluating' stamp: 'GB 10/22/2018 20:10:31'!
value

	lines := OrderedCollection new.
	level := 0.
	portfolio accept: self. 
	
	^ lines! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PortfolioTreePrinter class' category: #'PortfolioTreePrinter-Ejercicio'!
PortfolioTreePrinter class
	instanceVariableNames: ''!

!PortfolioTreePrinter class methodsFor: 'instance creation' stamp: 'GB 10/22/2018 20:08:33'!
of: aPortfolio namingAccountWith: aDictionary

	^ self new initializeOf: aPortfolio namingAccountWith: aDictionary ! !


!classDefinition: #SummarizingAccount category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #SummarizingAccount
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!SummarizingAccount methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:31:24'!
doesManage: anAccount

	self subclassResponsibility ! !

!SummarizingAccount methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:32:05'!
hasRegistered: aTransaction

	self subclassResponsibility ! !


!SummarizingAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:34'!
balance

	self subclassResponsibility ! !

!SummarizingAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:35'!
transactions

	self subclassResponsibility ! !


!SummarizingAccount methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:52:55'!
accept: aVisitor

	self subclassResponsibility ! !


!classDefinition: #Portfolio category: #'PortfolioTreePrinter-Ejercicio'!
SummarizingAccount subclass: #Portfolio
	instanceVariableNames: 'accounts'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!Portfolio methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:31:24'!
anyManagedAccountManages: anAccount 

	^accounts anySatisfy: [ :managedAccount | (managedAccount doesManage: anAccount) or: [ anAccount doesManage: managedAccount ] ] ! !

!Portfolio methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:31:24'!
doesManage: anAccount

	^ self = anAccount  or: [ self anyManagedAccountManages: anAccount ]! !

!Portfolio methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:32:05'!
hasRegistered: aTransaction

	^ accounts anySatisfy: [ :anAccount | anAccount hasRegistered: aTransaction ]  ! !


!Portfolio methodsFor: 'transactions' stamp: 'HAW 8/13/2017 17:47:44'!
balance

	^ accounts sum: [ :account | account balance ]
! !

!Portfolio methodsFor: 'transactions' stamp: 'HAW 9/25/2017 19:40:20'!
transactions 

	^ accounts 
		inject: OrderedCollection new 
		into: [ :transactions :account | transactions addAll: account transactions. transactions ]
	! !

!Portfolio methodsFor: 'transactions' stamp: 'HAW 10/18/2018 16:31:24'!
transactionsOf: anAccount 

	^ (self doesManage: anAccount)
		ifTrue: [ anAccount transactions ] 
		ifFalse: [ self error: self class accountNotManagedMessageDescription]
	! !


!Portfolio methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 19:19'!
initializeWithAll: aCollectionOfAccounts

	accounts := aCollectionOfAccounts   ! !


!Portfolio methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:48:43'!
accept: aVisitor

	aVisitor visitPortfolio: self! !

!Portfolio methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:59:15'!
visitAccountsWith: aVisitor

	accounts do: [:anAccount | anAccount accept: aVisitor ].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: #'PortfolioTreePrinter-Ejercicio'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'instance creation' stamp: 'HAW 5/8/2018 16:33:13'!
with: leftAccount with: rightAccount

	^ self withAll: (Array with: leftAccount with: rightAccount)! !

!Portfolio class methodsFor: 'instance creation' stamp: 'HAW 5/8/2018 16:40:55'!
withAll: aCollectionOfAccounts

	self checkCreationPreconditions: aCollectionOfAccounts.
	
	^self new initializeWithAll: aCollectionOfAccounts ! !


!Portfolio class methodsFor: 'assertions' stamp: 'HAW 10/18/2018 16:31:24'!
check: sourceAccount doesNotManagaAnyOf: aCollectionOfAccounts

	^ aCollectionOfAccounts do: [ :targetAccount | 
			(sourceAccount = targetAccount) not ifTrue: [ 
				(sourceAccount doesManage: targetAccount) ifTrue: [ self error: self accountAlreadyManagedErrorMessage ] ] ]! !

!Portfolio class methodsFor: 'assertions' stamp: 'HernanWilkinson 9/18/2011 17:21'!
checkAccountsAreUnique: aCollectionOfAccounts

	aCollectionOfAccounts asSet size = aCollectionOfAccounts size
		ifFalse: [ self error: self accountAlreadyManagedErrorMessage ]! !

!Portfolio class methodsFor: 'assertions' stamp: 'HernanWilkinson 9/18/2011 17:21'!
checkCreationPreconditions: aCollectionOfAccounts

	self checkAccountsAreUnique: aCollectionOfAccounts.
	self checkNoCircularReferencesIn: aCollectionOfAccounts! !

!Portfolio class methodsFor: 'assertions' stamp: 'HernanWilkinson 9/18/2011 17:22'!
checkNoCircularReferencesIn: aCollectionOfAccounts

	aCollectionOfAccounts do: [ :sourceAccount | self check: sourceAccount doesNotManagaAnyOf: aCollectionOfAccounts ]! !


!Portfolio class methodsFor: 'error messages' stamp: 'HernanWilkinson 7/13/2011 19:28'!
accountAlreadyManagedErrorMessage

	^ 'Account already managed'! !

!Portfolio class methodsFor: 'error messages' stamp: 'HernanWilkinson 7/13/2011 19:27'!
accountNotManagedMessageDescription
	
	^ 'Account not managed'! !


!classDefinition: #ReceptiveAccount category: #'PortfolioTreePrinter-Ejercicio'!
SummarizingAccount subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:35'!
initialize

	super initialize.
	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HAW 10/18/2018 20:20:37'!
balance

	^ transactions inject: 0 into: [ :balance :transaction | transaction affectBalance: balance ]! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:31:24'!
doesManage: anAccount

	^ self = anAccount 
! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'HAW 10/18/2018 16:32:05'!
hasRegistered: aTtransaction

	^ transactions includes: aTtransaction 
! !


!ReceptiveAccount methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:48:15'!
accept: aVisitor

	aVisitor visitReceptiveAccount: self! !

!ReceptiveAccount methodsFor: 'visiting' stamp: 'GB 10/22/2018 19:21:47'!
visitTransactionsWith: aVisitor

	transactions do: [:aTransaction | aTransaction accept: aVisitor ]! !


!classDefinition: #Transfer category: #'PortfolioTreePrinter-Ejercicio'!
Object subclass: #Transfer
	instanceVariableNames: 'value depositLeg withdrawLeg'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PortfolioTreePrinter-Ejercicio'!

!Transfer methodsFor: 'initialization' stamp: 'HAW 10/18/2018 21:08:52'!
initializeOf: anAmount 
	
	value := anAmount.
	depositLeg := TransferDeposit relatedTo: self .
	withdrawLeg := TransferWithdraw relatedTo: self 
	
	! !


!Transfer methodsFor: 'legs' stamp: 'HAW 10/18/2018 20:53:34'!
depositLeg
	
	^depositLeg! !

!Transfer methodsFor: 'legs' stamp: 'HAW 10/18/2018 20:53:26'!
withdrawLeg
	
	^withdrawLeg! !


!Transfer methodsFor: 'evaluating' stamp: 'HAW 10/18/2018 21:08:33'!
value

	^value ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Transfer class' category: #'PortfolioTreePrinter-Ejercicio'!
Transfer class
	instanceVariableNames: ''!

!Transfer class methodsFor: 'instance creation' stamp: 'HAW 10/18/2018 21:07:39'!
of: anAmount 

	^self new initializeOf: anAmount ! !


!Transfer class methodsFor: 'registering' stamp: 'HAW 10/18/2018 20:50:53'!
register: anAmount from: fromAccount to: toAccount 

	|transfer |
	
	transfer := self of: anAmount.
	fromAccount register: transfer withdrawLeg.
	toAccount register: transfer depositLeg.
	
	^transfer! !
