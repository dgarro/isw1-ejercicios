!classDefinition: #CantSuspend category: #'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: #'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: #'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: 'customerBook'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'DEG 9/24/2018 01:33:46'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds
	
	self assertThatEvaluate: [customerBook addCustomerNamed: 'John Lennon'.] takesAtMost: 50 * millisecond.
	
		
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'DEG 9/24/2018 01:37:21'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| paulMcCartney |
	
	paulMcCartney := 'Paul McCartney'.
	customerBook addCustomerNamed: paulMcCartney.

	self assertThatEvaluate: [customerBook removeCustomerNamed: paulMcCartney.] takesAtMost: 100 * millisecond.
	
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'DEG 9/24/2018 01:28:44'!
test03CanNotAddACustomerWithEmptyName 

	self evaluate: [customerBook addCustomerNamed: ''.] 
	expectError: Error 
	thenDo: [ :anError | 
						self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
						self assert: customerBook isEmpty ] 
	! !

!CustomerBookTest methodsFor: 'testing' stamp: 'DEG 9/24/2018 01:53:04'!
test04CanNotRemoveAnInvalidCustomer

	| johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self evaluate: [ customerBook removeCustomerNamed: 'Paul McCartney'.]  
	expectError: NotFound  
	thenDo: [ :anError | self assertThatCustomerNamed: johnLennon isTheOnlyCustomerIn: customerBook ] 
	! !

!CustomerBookTest methodsFor: 'testing' stamp: 'DEG 9/24/2018 01:50:53'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| paulMcCartney|
	
	paulMcCartney := 'Paul McCartney'.
	self addAndSuspendCustomerNamed: paulMcCartney in: customerBook.

	self assertThatIn: customerBook 
	theNumberOfCustomersIs: 1 
	theNumberOfActiveCustomersIs: 0 
	andTheNumberOfSuspendedCustomersIs: 1. 
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'DEG 9/24/2018 01:51:21'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| paulMcCartney|
	
	paulMcCartney := 'Paul McCartney'.
	self addAndSuspendCustomerNamed: paulMcCartney in: customerBook.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self assertThatIn: customerBook 
	theNumberOfCustomersIs: 0 
	theNumberOfActiveCustomersIs: 0 
	andTheNumberOfSuspendedCustomersIs: 0. 
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'DEG 9/24/2018 01:38:59'!
test07CanNotSuspendAnInvalidCustomer

	| johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.

	self assertThatCustomerNamed: 'Paul McCartney' canNotBeSuspendedIn: customerBook andTheOnlyCustomerIsNamed: johnLennon! !

!CustomerBookTest methodsFor: 'testing' stamp: 'DEG 9/24/2018 01:51:47'!
test08CanNotSuspendAnAlreadySuspendedCustomer

	| johnLennon |
			
	johnLennon := 'John Lennon'.
	self addAndSuspendCustomerNamed: johnLennon in: customerBook.

	self assertThatCustomerNamed: johnLennon canNotBeSuspendedIn: customerBook andTheOnlyCustomerIsNamed: johnLennon
! !


!CustomerBookTest methodsFor: 'initialization' stamp: 'DEG 9/24/2018 01:40:13'!
setUp
			
	customerBook := CustomerBook new.! !


!CustomerBookTest methodsFor: 'testing - private' stamp: 'DEG 9/24/2018 01:50:19'!
addAndSuspendCustomerNamed: aName in: aCustomerBook

	aCustomerBook addCustomerNamed: aName.
	aCustomerBook suspendCustomerNamed: aName.! !

!CustomerBookTest methodsFor: 'testing - private' stamp: 'DEG 9/24/2018 01:53:04'!
assertThatCustomerNamed: aNameOfCustomerToSuspend canNotBeSuspendedIn: aCustomerBook andTheOnlyCustomerIsNamed: aNameOfCustomerInCustomerBook
	
	self evaluate: [ aCustomerBook suspendCustomerNamed: aNameOfCustomerToSuspend ]
	expectError: CantSuspend
	thenDo: [ :anError | self assertThatCustomerNamed: aNameOfCustomerInCustomerBook isTheOnlyCustomerIn: aCustomerBook ].

	
! !

!CustomerBookTest methodsFor: 'testing - private' stamp: 'DEG 9/24/2018 01:53:04'!
assertThatCustomerNamed: aName isTheOnlyCustomerIn: aCustomerBook

	self assert: aCustomerBook numberOfCustomers = 1.
	self assert: (aCustomerBook includesCustomerNamed: aName)! !

!CustomerBookTest methodsFor: 'testing - private' stamp: 'GB 9/20/2018 20:58:36'!
assertThatEvaluate: aClousureToEvaluate takesAtMost: timeLimit

	| millisecondsAfterRunning millisecondsBeforeRunning |
	
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	aClousureToEvaluate value.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	self assert: (millisecondsAfterRunning-millisecondsBeforeRunning) < timeLimit! !

!CustomerBookTest methodsFor: 'testing - private' stamp: 'GB 9/22/2018 17:40:27'!
assertThatIn: aCustomerBook theNumberOfCustomersIs: aNumberOfCustomers theNumberOfActiveCustomersIs: aNumberActiveCustomers andTheNumberOfSuspendedCustomersIs: aNumberOfSuspendedCustomers
	
	self assert: aNumberActiveCustomers equals: aCustomerBook numberOfActiveCustomers.
	self assert: aNumberOfSuspendedCustomers equals: aCustomerBook numberOfSuspendedCustomers.
	self assert: aNumberOfCustomers equals: aCustomerBook numberOfCustomers.
! !

!CustomerBookTest methodsFor: 'testing - private' stamp: 'DEG 9/21/2018 21:31:06'!
evaluate: aClousureToEvaluate expectError: expectedError thenDo: aClosureWithAssertions

	[ aClousureToEvaluate value.
	self fail ]
		on: expectedError 
		do: aClosureWithAssertions ! !


!classDefinition: #CustomerBook category: #'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'DEG 9/23/2018 19:07:22'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'DEG 9/23/2018 19:07:22'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'DEG 9/23/2018 19:07:22'!
initialize

	super initialize.
	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'DEG 9/23/2018 19:07:22'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'DEG 9/23/2018 19:07:22'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'GB 9/21/2018 13:58:25'!
numberOfCustomers
	
	^ self numberOfActiveCustomers + self numberOfSuspendedCustomers ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'DEG 9/23/2018 19:07:22'!
removeCustomerNamed: aName 
 
	active  remove: aName 
	ifAbsent: [suspended remove:	 aName 
		ifAbsent: [^ NotFound signal]].
	
	^ aName 
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'DEG 9/23/2018 19:07:22'!
suspendCustomerNamed: aName 
	
	(active includes: aName) ifFalse: [^CantSuspend signal].
	
	active remove: aName.
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: #'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'HernanWilkinson 7/6/2011 17:57'!
customerAlreadyExistsErrorMessage

	^'Customer already exists'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'HernanWilkinson 7/6/2011 17:53'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty'! !
