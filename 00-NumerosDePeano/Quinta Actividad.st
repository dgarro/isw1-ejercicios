!classDefinition: #I category: #'Quinta Actividad'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Quinta Actividad'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: #'Quinta Actividad'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'as yet unclassified' stamp: 'GB 9/10/2018 16:13:38'!
* unNumeroDePeano

	^ unNumeroDePeano ! !

!I class methodsFor: 'as yet unclassified' stamp: 'ARM 9/6/2018 21:44:20'!
+ unNumeroDePeano

	^unNumeroDePeano next! !

!I class methodsFor: 'as yet unclassified' stamp: 'GB 9/12/2018 18:24:40'!
- unNumeroDePeano

	self error: self descripcionDeErrorDeNumerosNegativosNoSoportados ! !

!I class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:40:02'!
/ unNumeroDePeano

	unNumeroDePeano = self ifTrue: [^ self].
	self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor ! !

!I class methodsFor: 'as yet unclassified' stamp: 'GB 9/10/2018 16:24:20'!
<= unNumeroDePeano

	^ true ! !

!I class methodsFor: 'as yet unclassified' stamp: 'GB 9/12/2018 18:28:32'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	^ 'No se puede dividir por un numero mayor'! !

!I class methodsFor: 'as yet unclassified' stamp: 'GB 9/12/2018 18:24:46'!
descripcionDeErrorDeNumerosNegativosNoSoportados
	^ 'Numeros negativos no soportados'.! !

!I class methodsFor: 'as yet unclassified' stamp: 'ARM 9/6/2018 21:29:22'!
next

	^II! !

!I class methodsFor: 'as yet unclassified' stamp: 'GB 9/12/2018 18:14:20'!
restarA: unNumeroDePeano

	^ unNumeroDePeano previous ! !

!I class methodsFor: 'as yet unclassified' stamp: 'GB 9/10/2018 16:53:27'!
vecesDivideA: unNumeroDePeano

	^ unNumeroDePeano ! !


!classDefinition: #II category: #'Quinta Actividad'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Quinta Actividad'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: #'Quinta Actividad'!
II class
	instanceVariableNames: 'previous next'!

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/10/2018 20:31:42'!
* unNumeroDePeano

	^ unNumeroDePeano  + (self previous * unNumeroDePeano )! !

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/10/2018 20:31:42'!
+ unNumeroDePeano

	^self previous + unNumeroDePeano next! !

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/12/2018 18:13:24'!
- unNumeroDePeano

	^ unNumeroDePeano restarA: self
! !

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/12/2018 18:25:57'!
/ unNumeroDePeano

	unNumeroDePeano <= self ifFalse: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor ].
	^ unNumeroDePeano vecesDivideA: self! !

!II class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:40:54'!
<= unNumeroDePeano

	unNumeroDePeano  = I ifTrue: [^ false].
	self = unNumeroDePeano  ifTrue: [^ true].
	^ self <= unNumeroDePeano previous! !

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/12/2018 18:25:26'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	^ 'No se puede dividir por un numero mayor'.! !

!II class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:52:16'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := I.
	next := III.! !

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/10/2018 20:32:22'!
next
	
	next ifNil:
	[
		next _ self cloneNamed: self name , 'I'.
		next previous: self.
	].
	^next! !

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/10/2018 20:33:07'!
previous

	^previous! !

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/10/2018 20:33:07'!
previous: unNumeroDePeano

	previous _ unNumeroDePeano! !

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/12/2018 18:15:02'!
restarA: unNumeroDePeano

	^unNumeroDePeano previous - self previous! !

!II class methodsFor: 'as yet unclassified' stamp: 'GB 9/10/2018 20:31:42'!
vecesDivideA: unNumeroDePeano

	unNumeroDePeano <= (II * self ) previous ifTrue: [^ I].
	^ (self vecesDivideA: unNumeroDePeano - self ) + I! !


!classDefinition: #III category: #'Quinta Actividad'!
DenotativeObject subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Quinta Actividad'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: #'Quinta Actividad'!
III class
	instanceVariableNames: 'previous next'!

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
* unNumeroDePeano

	^ unNumeroDePeano  + (self previous * unNumeroDePeano )! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
+ unNumeroDePeano

	^self previous + unNumeroDePeano next! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
- unNumeroDePeano

	^ unNumeroDePeano restarA: self
! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
/ unNumeroDePeano

	unNumeroDePeano <= self ifFalse: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor ].
	^ unNumeroDePeano vecesDivideA: self! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
<= unNumeroDePeano

	unNumeroDePeano  = I ifTrue: [^ false].
	self = unNumeroDePeano  ifTrue: [^ true].
	^ self <= unNumeroDePeano previous! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	^ 'No se puede dividir por un numero mayor'.! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:52:16'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := II.
	next := IIII.! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
next
	
	next ifNil:
	[
		next _ self cloneNamed: self name , 'I'.
		next previous: self.
	].
	^next! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
previous

	^previous! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
previous: unNumeroDePeano

	previous _ unNumeroDePeano! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
restarA: unNumeroDePeano

	^unNumeroDePeano previous - self previous! !

!III class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
vecesDivideA: unNumeroDePeano

	unNumeroDePeano <= (II * self ) previous ifTrue: [^ I].
	^ (self vecesDivideA: unNumeroDePeano - self ) + I! !


!classDefinition: #IIII category: #'Quinta Actividad'!
DenotativeObject subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Quinta Actividad'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: #'Quinta Actividad'!
IIII class
	instanceVariableNames: 'previous next'!

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
* unNumeroDePeano

	^ unNumeroDePeano  + (self previous * unNumeroDePeano )! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
+ unNumeroDePeano

	^self previous + unNumeroDePeano next! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
- unNumeroDePeano

	^ unNumeroDePeano restarA: self
! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
/ unNumeroDePeano

	unNumeroDePeano <= self ifFalse: [self error: self descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor ].
	^ unNumeroDePeano vecesDivideA: self! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
<= unNumeroDePeano

	unNumeroDePeano  = I ifTrue: [^ false].
	self = unNumeroDePeano  ifTrue: [^ true].
	^ self <= unNumeroDePeano previous! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
descripcionDeErrorDeNoSePuedeDividirPorUnNumeroMayor
	^ 'No se puede dividir por un numero mayor'.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:52:16'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	previous := III.
	next := nil.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
next
	
	next ifNil:
	[
		next _ self cloneNamed: self name , 'I'.
		next previous: self.
	].
	^next! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
previous

	^previous! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
previous: unNumeroDePeano

	previous _ unNumeroDePeano! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
restarA: unNumeroDePeano

	^unNumeroDePeano previous - self previous! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'DEG 9/13/2018 12:42:13'!
vecesDivideA: unNumeroDePeano

	unNumeroDePeano <= (II * self ) previous ifTrue: [^ I].
	^ (self vecesDivideA: unNumeroDePeano - self ) + I! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!