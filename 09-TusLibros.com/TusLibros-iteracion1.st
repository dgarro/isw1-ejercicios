!classDefinition: #TusLibrosTest category: #TusLibros!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 17:50:17'!
test01ANewCartIsEmpty

	| cart |
	
	cart := self emptyCartWithCatalog.

	self assert: cart isEmpty ! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 17:46:52'!
test02ACartIsNotEmptyAfterAddingAnItemFromTheCatalog

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	cart addItem: self itemFromTheCatalog.
	
	self deny: cart isEmpty ! !

!TusLibrosTest methodsFor: 'testing' stamp: 'GB 11/15/2018 14:04:26'!
test03ACartContainsAnItemFromTheCatalogAfterAddingIt

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	cart addItem: self itemFromTheCatalog.
	
	self assert: (cart numberOf: self itemFromTheCatalog) equals: 1. ! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 17:47:36'!
test04ACartDoesNotContainsAnItemThatWasNotAddedInIt

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	self assert: (cart numberOf: self itemFromTheCatalog) equals: 0. 
	! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 17:59:15'!
test05TheNumberOfAnItemInACartIsTheNumberOfThatItemAddedInTheCart

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	cart addItem: self itemFromTheCatalog.	
	cart addItem: self itemFromTheCatalog.
	
	self assert: (cart numberOf: self itemFromTheCatalog) equals: 2. ! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 18:01:17'!
test06TheNumberOfAnItemInACartIsTheNumberOfThatItemAddedInTheCartForDifferentItems

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	cart addItem: self itemFromTheCatalog.	
	cart addItem: self itemFromTheCatalog.
	cart addItem: self anotherItemFromTheCatalog.
	
	self assert: (cart numberOf: self itemFromTheCatalog) equals: 2.
	self assert: (cart numberOf: self anotherItemFromTheCatalog) equals: 1. ! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 17:39:26'!
test07AnEmptyCartHasZeroItems

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	self assert: cart numberOfItems equals: 0
! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 18:01:55'!
test08TheNumberOfItemsInACartIsTheNumberOfAddedItems

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	cart addItem: self itemFromTheCatalog.	
	cart addItem: self itemFromTheCatalog.
	cart addItem: self anotherItemFromTheCatalog.
	
	self assert: cart numberOfItems equals: 3! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 18:07:36'!
test09CanNotAddAnItemThatIsNotFromTheCatalog

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	self
		should: [cart addItem: self itemNotFromTheCatalog]
		raise: self functionalException
		withExceptionDo: [ :anError | 
			self assert: Cart canNotAddAnItemThatIsNotFromTheCatalog equals: anError messageText. 
			self assert: cart isEmpty ].! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 18:35:59'!
test10ACartContainsANumberOfAnItemThatWasAddedAtOnce

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	cart add: 10 of: self itemFromTheCatalog.
	
	self assert: (cart numberOf: self itemFromTheCatalog) equals: 10.
	self assert: cart numberOfItems equals: 10 ! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 18:10:44'!
test11CanNotAddANumberOfAnItemThatIsNotFromTheCatalog

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	self
		should: [cart add: 10 of: self itemNotFromTheCatalog]
		raise: self functionalException
		withExceptionDo: [ :anError | 
			self assert: Cart canNotAddAnItemThatIsNotFromTheCatalog equals: anError messageText. 
			self assert: cart isEmpty ].! !

!TusLibrosTest methodsFor: 'testing' stamp: 'DEG 11/13/2018 18:45:56'!
test12CanNotAddANonPositiveNumberOfAnItem

	| cart |
	
	cart := self emptyCartWithCatalog.
	
	self
		should: [cart add: 0 of: self itemFromTheCatalog]
		raise: self functionalException
		withExceptionDo: [ :anError | 
			self assert: Cart canNotAddANonPositiveNumberOfAnItems equals: anError messageText. 
			self assert: cart isEmpty ].! !


!TusLibrosTest methodsFor: 'test support' stamp: 'DEG 11/13/2018 17:29:30'!
anotherItemFromTheCatalog

	^'isbn2'! !

!TusLibrosTest methodsFor: 'test support' stamp: 'DEG 11/13/2018 17:29:30'!
emptyCartWithCatalog

	^Cart with: (OrderedCollection with: self itemFromTheCatalog with: self anotherItemFromTheCatalog)! !

!TusLibrosTest methodsFor: 'test support' stamp: 'GB 11/12/2018 21:25:05'!
functionalException

	^ Error - MessageNotUnderstood.! !

!TusLibrosTest methodsFor: 'test support' stamp: 'DEG 11/13/2018 17:29:20'!
itemFromTheCatalog

	^'isbn1'! !

!TusLibrosTest methodsFor: 'test support' stamp: 'DEG 11/13/2018 17:29:46'!
itemNotFromTheCatalog

	^'isbn3'! !


!classDefinition: #Cart category: #TusLibros!
Object subclass: #Cart
	instanceVariableNames: 'items catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'item counting' stamp: 'DEG 11/13/2018 17:38:40'!
numberOf: anItem 
	
	^ items occurrencesOf: anItem ! !

!Cart methodsFor: 'item counting' stamp: 'DEG 11/13/2018 17:39:26'!
numberOfItems
	
	^ items size! !


!Cart methodsFor: 'testing' stamp: 'GB 11/12/2018 20:43:41'!
isEmpty
	
	^ items isEmpty ! !


!Cart methodsFor: 'initialization' stamp: 'GB 11/12/2018 21:20:50'!
initializeWith: aCatalog
	
	catalog := aCatalog.
	items := OrderedCollection new
! !


!Cart methodsFor: 'item adding' stamp: 'DEG 11/13/2018 18:45:34'!
add: aNumberOfItems of: anItem
	
	(aNumberOfItems > 0) ifFalse: [self error: self class canNotAddANonPositiveNumberOfAnItems].
	
	aNumberOfItems timesRepeat: [self addItem: anItem]
	
	! !

!Cart methodsFor: 'item adding' stamp: 'DEG 11/13/2018 18:36:28'!
addItem: anItem 

	(catalog includes: anItem) ifFalse: [self error: self class canNotAddAnItemThatIsNotFromTheCatalog].
	
	items add: anItem! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: #TusLibros!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'error messages' stamp: 'DEG 11/13/2018 18:45:14'!
canNotAddANonPositiveNumberOfAnItems

	^ 'Can not add a negative number of an item'! !

!Cart class methodsFor: 'error messages' stamp: 'DEG 11/13/2018 17:56:53'!
canNotAddAnItemThatIsNotFromTheCatalog

	^ 'Can not add an item that is not from the catalog'! !


!Cart class methodsFor: 'instance creation' stamp: 'GB 11/12/2018 21:20:22'!
with: aCatalog

	^ self new initializeWith: aCatalog ! !
