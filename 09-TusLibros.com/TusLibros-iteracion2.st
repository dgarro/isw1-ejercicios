!classDefinition: #TusLibrosTest category: #TusLibros!
TestCase subclass: #TusLibrosTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TusLibrosTest methodsFor: 'support' stamp: 'DEG 11/15/2018 20:29:42'!
anotherItemSellByTheStore
	
	^ 'anotherValidBook'! !

!TusLibrosTest methodsFor: 'support' stamp: 'DEG 11/15/2018 20:29:47'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!TusLibrosTest methodsFor: 'support' stamp: 'GB 11/22/2018 14:28:42'!
createCashier

	^ Cashier withDebitProcessor: self! !

!TusLibrosTest methodsFor: 'support' stamp: 'GB 11/21/2018 15:34:52'!
creditCardNumber
	
	^ '5400000000000001'! !

!TusLibrosTest methodsFor: 'support' stamp: 'GB 11/21/2018 15:35:31'!
creditCardOwner
	
	^ 'Facundo R.'! !

!TusLibrosTest methodsFor: 'support' stamp: 'DEG 11/21/2018 17:19:42'!
currentMonthOfYear
	
	^ November, 2018! !

!TusLibrosTest methodsFor: 'support' stamp: 'DEG 11/15/2018 20:29:50'!
defaultCatalog
	
	| catalog |
	
	catalog  := Dictionary new.
	catalog at: self itemSellByTheStore put: 10;
			 at: self anotherItemSellByTheStore put: 15.
	
	^ catalog ! !

!TusLibrosTest methodsFor: 'support' stamp: 'DEG 11/21/2018 17:09:58'!
expiredCreditCard

	^ CreditCard withNumber: self creditCardNumber 
					 withExpiration: self currentMonthOfYear previous
					 withOwner: self creditCardOwner! !

!TusLibrosTest methodsFor: 'support' stamp: 'DEG 11/15/2018 20:29:53'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!TusLibrosTest methodsFor: 'support' stamp: 'DEG 11/15/2018 20:29:56'!
itemSellByTheStore
	
	^ 'validBook'! !

!TusLibrosTest methodsFor: 'support' stamp: 'DEG 11/21/2018 17:11:12'!
notExpiredCreditCard

	^ CreditCard withNumber: self creditCardNumber
					 withExpiration: self currentMonthOfYear
					 withOwner: self creditCardOwner! !


!classDefinition: #CartTest category: #TusLibros!
TusLibrosTest subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:08'!
test01NewCartsAreCreatedEmpty

	self assert: self createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [ cart add: self itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 0 of: self itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 2 of: self itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test06CartRemembersAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: self itemSellByTheStore.
	self assert: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:43'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := self createCart.
	
	self deny: (cart includes: self itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:45'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: 2 of: self itemSellByTheStore.
	self assert: (cart occurrencesOf: self itemSellByTheStore) = 2! !


!classDefinition: #CashierTest category: #TusLibros!
TusLibrosTest subclass: #CashierTest
	instanceVariableNames: 'debitProcess'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'DEG 11/22/2018 14:43:43'!
test01CanNotCheckoutAnEmptyCart

	| aCashier passedThroughDebitProcessor |
	
	aCashier := self createCashier.
	passedThroughDebitProcessor := false.
	debitProcess := [:anAmount :aCreditCard | passedThroughDebitProcessor := true].
	
	self 
		should: [aCashier checkout: self createCart withCreditCard: self notExpiredCreditCard at: self currentMonthOfYear ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = aCashier canNotCheckoutEmptyCartErrorMessage.
			self assert: aCashier didNotSellAnyItem.
			self deny: passedThroughDebitProcessor]! !

!CashierTest methodsFor: 'tests' stamp: 'GB 11/21/2018 16:59:00'!
test02CheckoutOfOneItemProducesATicketWithItsPrice

	| aCashier aCart |
	
	aCart := self createCart.
	aCashier := self createCashier.

	aCart add: self itemSellByTheStore.
	
	self assert: (aCashier checkout: aCart withCreditCard: self notExpiredCreditCard at: self currentMonthOfYear) equals: 10.! !

!CashierTest methodsFor: 'tests' stamp: 'GB 11/21/2018 16:59:05'!
test03CheckoutOfSeveralItemsProducesATicketWithTheSumOfTheirPrices

	| aCashier aCart |
	
	aCart := self createCart.
	aCashier := self createCashier.
	
	aCart add: self itemSellByTheStore.
	aCart add: 2 of: self anotherItemSellByTheStore.
	
	self assert: (aCashier checkout: aCart withCreditCard: self notExpiredCreditCard at: self currentMonthOfYear) equals: 40.! !

!CashierTest methodsFor: 'tests' stamp: 'GB 11/21/2018 16:59:10'!
test04CashierSalesBookRemembersASoldItem

	| aCashier aCart |
	
	aCart := self createCart.
	aCashier := self createCashier.

	aCart add: self itemSellByTheStore.
	aCashier checkout: aCart withCreditCard: self notExpiredCreditCard at: self currentMonthOfYear.
	
	self deny: aCashier didNotSellAnyItem.
	self assert: (aCashier numberOfSalesOf: self itemSellByTheStore) equals: 1.
	self assert: (aCashier totalNumberOfSoldItems) equals: 1.
	! !

!CashierTest methodsFor: 'tests' stamp: 'GB 11/21/2018 16:59:15'!
test05CashierSaleBookRemembersSeveralSoldItems

	| aCashier aCart |
	
	aCart := self createCart.
	aCashier := self createCashier.
	
	aCart add: self itemSellByTheStore.
	aCart add: 2 of: self anotherItemSellByTheStore.
	aCashier checkout: aCart withCreditCard: self notExpiredCreditCard at: self currentMonthOfYear.
	
	self assert: (aCashier numberOfSalesOf: self itemSellByTheStore) equals: 1.
	self assert: (aCashier numberOfSalesOf: self anotherItemSellByTheStore ) equals: 2.
	self assert: (aCashier totalNumberOfSoldItems) equals: 3

! !

!CashierTest methodsFor: 'tests' stamp: 'DEG 11/22/2018 14:44:02'!
test06CanNotCheckoutWithAnExpiredCreditCard

	| aCashier aCart passedThroughDebitProcessor |
	
	aCart := self createCart.
	aCashier := self createCashier.
	passedThroughDebitProcessor := false.
	debitProcess := [:anAmount :aCreditCard | passedThroughDebitProcessor := true].

	aCart add: self itemSellByTheStore.
	
	self 
		should: [aCashier checkout: aCart withCreditCard: self expiredCreditCard at: self currentMonthOfYear ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = aCashier canNotCheckoutWithAnExpiredCreditCardErrorMessage.
			self assert: aCashier didNotSellAnyItem.
			self deny: passedThroughDebitProcessor]! !

!CashierTest methodsFor: 'tests' stamp: 'GB 11/22/2018 14:17:05'!
test07AValidCheckoutDebitsTheCorrectAmountFromTheCorrectCreditCard

	| aCashier aCart debitedAmount debitedCreditCard aCreditCardToDebit |
	
	aCart := self createCart.
	aCashier := self createCashier.
	debitProcess := [ :anAmount :aCreditCard | debitedAmount := anAmount. debitedCreditCard := aCreditCard].
	aCreditCardToDebit := self notExpiredCreditCard.
	 
	aCart add: self itemSellByTheStore.
	aCashier checkout: aCart withCreditCard: aCreditCardToDebit at: self currentMonthOfYear.
	
	self assert: debitedAmount equals: 10.
	self assert: debitedCreditCard equals: aCreditCardToDebit ! !

!CashierTest methodsFor: 'tests' stamp: 'DEG 11/22/2018 14:44:14'!
test08CanNotCheckoutWithAStolenCreditCard

	| aCashier aCart passedThroughDebitProcessor |
	
	aCart := self createCart.
	aCashier := self createCashier.
	passedThroughDebitProcessor := false.
	debitProcess := [:anAmount :aCreditCard | passedThroughDebitProcessor := true. self error: 'Stolen credit card'].

	aCart add: self itemSellByTheStore.
	
	self 
		should: [aCashier checkout: aCart withCreditCard: self notExpiredCreditCard at: self currentMonthOfYear ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = 'Stolen credit card'.
			self assert: aCashier didNotSellAnyItem.
			self assert: passedThroughDebitProcessor]! !


!CashierTest methodsFor: 'initialization' stamp: 'GB 11/22/2018 14:17:05'!
setUp

	debitProcess := [ :anAmount :aCreditCard | ]! !


!CashierTest methodsFor: 'simulating' stamp: 'GB 11/22/2018 14:17:05'!
debit: anAmount from: aCreditCard

	debitProcess value: anAmount value: aCreditCard ! !


!classDefinition: #Cart category: #TusLibros!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'DEG 11/15/2018 20:28:34'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !

!Cart methodsFor: 'queries' stamp: 'DEG 11/15/2018 20:34:45'!
summarize

	^ items inject: 0 into: [ :subTotal :anItem | subTotal + (catalog at: anItem)]

	! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !


!Cart methodsFor: 'iterating' stamp: 'DEG 11/15/2018 20:54:20'!
itemsDo: aBlock

	items do: aBlock ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: #TusLibros!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: #TusLibros!
Object subclass: #Cashier
	instanceVariableNames: 'salesBook debitProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'error messages' stamp: 'DEG 11/15/2018 18:52:10'!
canNotCheckoutEmptyCartErrorMessage

	^ 'Can not checkout an empty cart'! !

!Cashier methodsFor: 'error messages' stamp: 'GB 11/21/2018 15:55:43'!
canNotCheckoutWithAnExpiredCreditCardErrorMessage

	^ 'Can not checkout with an expired credit card'! !


!Cashier methodsFor: 'initialization' stamp: 'DEG 11/22/2018 14:46:16'!
initializeWithDebitProcessor: aDebitProcessor

	debitProcessor := aDebitProcessor.
	salesBook := OrderedCollection new.! !


!Cashier methodsFor: 'queries' stamp: 'GB 11/21/2018 15:19:30'!
numberOfSalesOf: anItem

	^ salesBook occurrencesOf: anItem ! !

!Cashier methodsFor: 'queries' stamp: 'GB 11/21/2018 15:19:02'!
totalNumberOfSoldItems

	^ salesBook size! !


!Cashier methodsFor: 'testing' stamp: 'GB 11/21/2018 15:18:42'!
didNotSellAnyItem
	
	^ salesBook isEmpty ! !


!Cashier methodsFor: 'operations' stamp: 'DEG 11/22/2018 14:44:29'!
checkout: aCart withCreditCard: aCreditCard at: currentMonthOfYear

	| ticketAmount |
	
	self assertIsValidCart: aCart.
	self assertIsValidCreditCard: aCreditCard at: currentMonthOfYear.
		
	ticketAmount := aCart summarize.
	debitProcessor debit: ticketAmount from: aCreditCard.
	aCart itemsDo: [ :anItem | salesBook add: anItem]. 
	
	^ ticketAmount ! !


!Cashier methodsFor: 'assertions' stamp: 'DEG 11/21/2018 17:23:04'!
assertIsValidCart: aCart 
	
	aCart isEmpty ifTrue: [self error: self canNotCheckoutEmptyCartErrorMessage].
! !

!Cashier methodsFor: 'assertions' stamp: 'DEG 11/22/2018 14:44:29'!
assertIsValidCreditCard: aCreditCard at: currentMonthOfYear
			
	(aCreditCard isExpiredAt: currentMonthOfYear)
		ifTrue: [self error: self canNotCheckoutWithAnExpiredCreditCardErrorMessage].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: #TusLibros!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'instance creation' stamp: 'DEG 11/22/2018 14:46:49'!
withDebitProcessor: aDebitProcessor

	^ self new initializeWithDebitProcessor: aDebitProcessor! !


!classDefinition: #CreditCard category: #TusLibros!
Object subclass: #CreditCard
	instanceVariableNames: 'number expiration owner'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'GB 11/21/2018 16:00:07'!
isExpiredAt: aMonthOfYear

	^ expiration < aMonthOfYear ! !


!CreditCard methodsFor: 'initialization' stamp: 'GB 11/21/2018 15:37:45'!
initializeWithNumber: aCreditCardNumber withExpiration: anExpirationMonthOfYear withOwner: aName 

	number := aCreditCardNumber.
	expiration := anExpirationMonthOfYear.
	owner := aName! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: #TusLibros!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'GB 11/21/2018 15:36:48'!
withNumber: aCreditCardNumber withExpiration: anExpirationMonthOfYear withOwner: aName 
	
	^ self new initializeWithNumber: aCreditCardNumber withExpiration: anExpirationMonthOfYear withOwner: aName ! !
