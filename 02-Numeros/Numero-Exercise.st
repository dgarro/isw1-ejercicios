!classDefinition: #NumeroTest category: #'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'GB 9/26/2018 16:24:17'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)
	
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:26'!
test27EnteroSubstractsEnteroCorrectly

	self assert: two - one equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: #'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'DEG 9/28/2018 20:09:16'!
- aSubtrahend

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !


!Numero methodsFor: 'testing' stamp: 'NR 9/23/2018 23:36:49'!
isNegative

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isOne

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isZero

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: #'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'HernanWilkinson 5/7/2016 22:45'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero'! !

!Numero class methodsFor: 'error descriptions' stamp: 'HernanWilkinson 5/7/2016 22:47'!
invalidNumberTypeErrorDescription
	^ 'Tipo de numero invalido'! !


!classDefinition: #Entero category: #'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic operations' stamp: 'GB 9/26/2018 16:33:22'!
* aMultiplier 
	
	^ aMultiplier multiplyEntero: self
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'GB 9/26/2018 15:57:27'!
+ anAdder 
	
	^ anAdder addEntero: self
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'DEG 9/28/2018 20:14:55'!
- aSubtrahend 
	
	^ aSubtrahend  substractToEntero: self! !

!Entero methodsFor: 'arithmetic operations' stamp: 'GB 9/26/2018 16:43:23'!
/ aDivisor 
	
	^ aDivisor divideEntero: self! !

!Entero methodsFor: 'arithmetic operations' stamp: 'DEG 9/30/2018 19:22:08'!
// aDivisor 
	
	^self class with: self integerValue // aDivisor integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'GB 9/27/2018 21:29:56'!
fibonacci

	self subclassResponsibility 
		! !

!Entero methodsFor: 'arithmetic operations' stamp: 'DEG 9/30/2018 19:22:16'!
greatestCommonDivisorWith: anEntero 
	
	^self class with: (self integerValue gcd: anEntero integerValue)! !


!Entero methodsFor: 'comparing' stamp: 'DEG 9/30/2018 19:22:29'!
= anObject

	^(anObject isKindOf: self class) and: [ self integerValue = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'DEG 9/30/2018 19:22:39'!
hash

	^self integerValue hash! !


!Entero methodsFor: 'value' stamp: 'DEG 9/30/2018 19:09:04'!
integerValue

	self subclassResponsibility ! !


!Entero methodsFor: 'printing' stamp: 'DEG 9/30/2018 19:22:53'!
printOn: aStream

	aStream print: self integerValue ! !


!Entero methodsFor: 'testing' stamp: 'DEG 9/30/2018 19:23:28'!
isNegative
	
	^ self integerValue < 0! !

!Entero methodsFor: 'testing' stamp: 'DEG 9/30/2018 19:23:44'!
isOne
	
	^ self integerValue = 1! !

!Entero methodsFor: 'testing' stamp: 'DEG 9/30/2018 19:23:50'!
isZero
	
	^ self integerValue = 0! !


!Entero methodsFor: 'arithmetic operations - double dispatch' stamp: 'DEG 9/30/2018 19:24:07'!
addEntero: anEnteroAdder

	^ self class with: self integerValue + anEnteroAdder integerValue ! !

!Entero methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:12:43'!
addFraccion: aFraccionAdder

	^ aFraccionAdder addEntero: self! !

!Entero methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:42:57'!
divideEntero: anEnteroDividend 
	
	^Fraccion with: anEnteroDividend over: self! !

!Entero methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:51:46'!
divideFraccion: aFraccionDividend

	^ aFraccionDividend numerator / (aFraccionDividend denominator * self)! !

!Entero methodsFor: 'arithmetic operations - double dispatch' stamp: 'DEG 9/30/2018 19:24:27'!
multiplyEntero: anEnteroMultiplier 
	
	^self class with: self integerValue * anEnteroMultiplier integerValue
	! !

!Entero methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:36:37'!
multiplyFraccion: aFraccionMultiplier

	^ aFraccionMultiplier multiplyEntero: self! !

!Entero methodsFor: 'arithmetic operations - double dispatch' stamp: 'DEG 9/30/2018 19:24:35'!
substractToEntero: anEnteroMinuend 
	
	^ self class with: anEnteroMinuend integerValue - self integerValue ! !

!Entero methodsFor: 'arithmetic operations - double dispatch' stamp: 'DEG 9/28/2018 20:24:30'!
substractToFraccion: aFraccionMinuend

	^ aFraccionMinuend numerator - (self * aFraccionMinuend denominator) / aFraccionMinuend denominator ! !


!Entero methodsFor: 'initialization - private' stamp: 'GB 9/29/2018 21:25:06'!
withNumerator: aNumerator

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: #'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'GB 9/30/2018 22:12:34'!
with: aValue 

	|subclass|
		
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	
	subclass := Entero subclasses detect: [:aSubclass | aSubclass represents: aValue ].
	
	subclass representsASingleValue 
		ifTrue: [^ subclass new]  
		ifFalse: [^ subclass new initalizeWith: aValue] 
! !


!Entero class methodsFor: 'error descriptions' stamp: 'NR 9/23/2018 22:17:16'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no est� definido aqu� para enteros negativos'! !


!Entero class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:12:34'!
represents: aValue

	self subclassResponsibility ! !

!Entero class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:09:11'!
representsASingleValue

	self subclassResponsibility ! !


!classDefinition: #Cero category: #'Numero-Exercise'!
Entero subclass: #Cero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Cero methodsFor: 'arithmetic operations' stamp: 'GB 9/27/2018 21:27:02'!
fibonacci

	^ Entero with: 1! !


!Cero methodsFor: 'value' stamp: 'DEG 9/30/2018 19:09:23'!
integerValue

	^ 0! !


!Cero methodsFor: 'initialization - private' stamp: 'GB 9/29/2018 21:23:40'!
withNumerator: aNumerator

	self error: self class canNotDivideByZeroErrorDescription
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cero class' category: #'Numero-Exercise'!
Cero class
	instanceVariableNames: ''!

!Cero class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:12:34'!
represents: aValue

	^ aValue = 0! !

!Cero class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:09:28'!
representsASingleValue

	^ true! !


!classDefinition: #EnteroMayorAUno category: #'Numero-Exercise'!
Entero subclass: #EnteroMayorAUno
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroMayorAUno methodsFor: 'arithmetic operations' stamp: 'GB 9/27/2018 21:29:41'!
fibonacci
	|one two|
	one := Entero with: 1.
	two := Entero with: 2.
	^ (self - one) fibonacci + (self - two) fibonacci! !


!EnteroMayorAUno methodsFor: 'value' stamp: 'DEG 9/30/2018 19:11:42'!
integerValue

	^ value! !


!EnteroMayorAUno methodsFor: 'initialization - private' stamp: 'DEG 9/30/2018 19:57:58'!
initalizeWith: aValue

	value := aValue ! !

!EnteroMayorAUno methodsFor: 'initialization - private' stamp: 'GB 9/29/2018 21:24:13'!
withNumerator: aNumerator

	^ Fraccion new initializeWith: aNumerator over: self ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroMayorAUno class' category: #'Numero-Exercise'!
EnteroMayorAUno class
	instanceVariableNames: ''!

!EnteroMayorAUno class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:12:34'!
represents: aValue

	^ aValue > 1! !

!EnteroMayorAUno class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:09:37'!
representsASingleValue

	^ false ! !


!classDefinition: #EnteroNegativo category: #'Numero-Exercise'!
Entero subclass: #EnteroNegativo
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroNegativo methodsFor: 'arithmetic operations' stamp: 'DEG 9/28/2018 19:49:36'!
fibonacci

	self error: self class negativeFibonacciErrorDescription ! !


!EnteroNegativo methodsFor: 'value' stamp: 'DEG 9/30/2018 19:12:05'!
integerValue

	^ value! !


!EnteroNegativo methodsFor: 'initialization - private' stamp: 'DEG 9/30/2018 19:58:03'!
initalizeWith: aValue

	value := aValue ! !

!EnteroNegativo methodsFor: 'initialization - private' stamp: 'DEG 9/30/2018 20:20:52'!
withNumerator: aNumerator

	^ Fraccion new initializeWith: aNumerator negated over: self negated! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroNegativo class' category: #'Numero-Exercise'!
EnteroNegativo class
	instanceVariableNames: ''!

!EnteroNegativo class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:12:34'!
represents: aValue

	^ aValue < 0! !

!EnteroNegativo class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:09:52'!
representsASingleValue

	^ false ! !


!classDefinition: #Uno category: #'Numero-Exercise'!
Entero subclass: #Uno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Uno methodsFor: 'arithmetic operations' stamp: 'DEG 9/28/2018 19:11:50'!
fibonacci

	^ self! !


!Uno methodsFor: 'value' stamp: 'DEG 9/30/2018 19:12:33'!
integerValue

	^ 1! !


!Uno methodsFor: 'initialization - private' stamp: 'GB 9/29/2018 21:24:46'!
withNumerator: aNumerator

	^ aNumerator! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Uno class' category: #'Numero-Exercise'!
Uno class
	instanceVariableNames: ''!

!Uno class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:12:34'!
represents: aValue

	^ aValue = 1! !

!Uno class methodsFor: 'testing' stamp: 'GB 9/30/2018 22:09:59'!
representsASingleValue

	^ true! !


!classDefinition: #Fraccion category: #'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'arithmetic operations' stamp: 'GB 9/26/2018 16:34:39'!
* aMultiplier 
	
	^ aMultiplier multiplyFraccion: self
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'GB 9/26/2018 16:28:58'!
+ anAdder 
	
	^ anAdder addFraccion: self! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'DEG 9/28/2018 20:16:48'!
- aSubtrahend 

	^ aSubtrahend substractToFraccion: self! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'GB 9/26/2018 16:49:22'!
/ aDivisor 
	
	^ aDivisor divideFraccion: self! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization - private' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'NR 9/23/2018 23:41:38'!
isNegative
	
	^numerator < 0! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !


!Fraccion methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:07:07'!
addEntero: anEnteroAdder

	^ numerator + (anEnteroAdder * denominator) / denominator ! !

!Fraccion methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:13:12'!
addFraccion: aFraccionAdder 
	
	| newNumerator newDenominator |
	
	newNumerator := (numerator * aFraccionAdder denominator) + (denominator * aFraccionAdder numerator).
	newDenominator := denominator * aFraccionAdder denominator.
	
	^newNumerator / newDenominator ! !

!Fraccion methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:46:40'!
divideEntero: anEnteroDividend

	^ denominator * anEnteroDividend / numerator ! !

!Fraccion methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:48:56'!
divideFraccion: aFraccionDividend 
	
	^ (denominator * aFraccionDividend numerator) / (numerator * aFraccionDividend denominator) ! !

!Fraccion methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:31:47'!
multiplyEntero: anEnteroMultiplier

	^ numerator * anEnteroMultiplier / denominator ! !

!Fraccion methodsFor: 'arithmetic operations - double dispatch' stamp: 'GB 9/26/2018 16:33:59'!
multiplyFraccion: aFraccionMultiplier 
	
	^(numerator * aFraccionMultiplier numerator) / (denominator * aFraccionMultiplier denominator)
	! !

!Fraccion methodsFor: 'arithmetic operations - double dispatch' stamp: 'DEG 9/28/2018 20:21:25'!
substractToEntero: anEnteroMinuend
	
	^  (anEnteroMinuend * denominator) - numerator / denominator ! !

!Fraccion methodsFor: 'arithmetic operations - double dispatch' stamp: 'DEG 9/28/2018 20:19:27'!
substractToFraccion: aFraccionMinuend
	
	| newNumerator newDenominator |
	
	newNumerator :=  (denominator * aFraccionMinuend numerator) - (numerator * aFraccionMinuend denominator).
	newDenominator := denominator * aFraccionMinuend denominator.
	
	^newNumerator / newDenominator! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: #'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'GB 9/29/2018 21:25:16'!
with: aDividend over: aDivisor

	| greatestCommonDivisor numerator denominator |
	
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: aDivisor. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := aDivisor // greatestCommonDivisor.
	
	^ denominator withNumerator: numerator.
	! !
