!classDefinition: #TerniLapilliTest category: #TerniLapilli!
TestCase subclass: #TerniLapilliTest
	instanceVariableNames: 'terniLapilli'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilliTest methodsFor: 'tests' stamp: 'GB 11/6/2018 17:20:06'!
test01NewGameBoardIsEmpty
	
	self assert: (terniLapilli isEmpty) ! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'GB 11/6/2018 17:21:00'!
test02AfterPuttingATokenTheBoardIsNotEmpty

	terniLapilli putAt: 1@1.
	
	self deny: (terniLapilli isEmpty) ! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/6/2018 22:16:20'!
test03CanNotAskIsEmptyAtARowBiggerThanTheBound
	
	self 
		should: [ terniLapilli isEmptyAt: 4@1 ] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli positionOutOfBoundMessageDescription = error messageText]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/6/2018 22:16:31'!
test04CanNotAskIsEmptyAtAColumnBiggerThanTheBound
	
	self 
		should: [ terniLapilli isEmptyAt: 1@4 ] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli positionOutOfBoundMessageDescription = error messageText]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/6/2018 22:16:45'!
test05CanNotAskIsEmptyAtARowSmallerThanTheBound
	
	self 
		should: [ terniLapilli isEmptyAt: 0@1 ] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli positionOutOfBoundMessageDescription = error messageText]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/6/2018 22:17:01'!
test06CanNotAskAtEmptyToAColumnSmallerThanTheBound
	
	self 
		should: [ terniLapilli isEmptyAt: 1@0 ] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli positionOutOfBoundMessageDescription = error messageText]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'GB 11/6/2018 17:28:05'!
test07AfterPuttingATokenInAPositionThatPositionIsNotEmpty
	
	terniLapilli putAt: 1@1.
	
	self deny: (terniLapilli isEmptyAt: 1@1).
! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'GB 11/6/2018 17:30:38'!
test08AfterPuttingAXTokenInAPositionThereIsAXTokenInThatPosition
	
	terniLapilli putAt: 1@1.
	
	self assert: (terniLapilli isXAt: 1@1)
! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'GB 11/6/2018 17:30:11'!
test09AfterPuttingAXTokenInAPositionThereIsNotAnOTokenInThatPosition
	
	terniLapilli putAt: 1@1.
	
	self deny: (terniLapilli isOAt: 1@1)
! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'GB 11/6/2018 17:32:04'!
test10ThereIsNoTokensInEmptyPositions
	
	self deny: (terniLapilli isXAt: 1@1).
	self deny: (terniLapilli isOAt: 1@1)
! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'GB 11/6/2018 17:33:01'!
test11AfterPuttingAOTokenInAPositionThereIsAOTokenInThatPosition
	
	terniLapilli putAt: 1@1. 
	terniLapilli putAt: 1@2.
	
	self assert: (terniLapilli isOAt: 1@2)
! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:18'!
test12AfterPuttingAOTokenInAPositionThereIsNotAnXTokenInThatPosition
	
	self putAllAt: (Array with: 1@1 with: 1@2).
	
	self deny: (terniLapilli isXAt: 1@2)
! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/6/2018 22:17:18'!
test13CanNotPutATokenAtAPositionOutOfBound
	
	self 
		should: [ terniLapilli putAt: 4@4 ] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli positionOutOfBoundMessageDescription = error messageText]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/6/2018 22:17:32'!
test14CanNotPutATokenAtANotEmptyPosition

	terniLapilli putAt: 1@1.
	
	self 
		should: [ terniLapilli putAt: 1@1 ] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli notEmptyPositionMessageDescription = error messageText.
			self assert: (terniLapilli isXAt: 1@1)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:24'!
test15CanNotPutATokenWhenAllTokensAreInTheBoard

	self putAllAt: (Array with: 1@1 with: 1@2 with: 1@3 with: 2@1 with: 2@2 with:2@3).
	
	self 
		should: [ terniLapilli putAt: 3@2 ] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli allTokensInBoardMessageDescription = error messageText.
			self assert: (terniLapilli isEmptyAt: 3@2) ]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:28'!
test16XCanWinByFillingTheFirstColumn
	
	self putAllAt: (Array with: 1@1 with: 1@2 with: 2@1 with: 1@3 with:3@1).
	
	self assert: (terniLapilli isXWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/6/2018 22:22:19'!
test17ThereIsNoWinnerInANewGame
	
	self deny: (terniLapilli isXWinner).
	self deny: (terniLapilli isOWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:31'!
test18XCanWinByFillingTheSecondColumn
	
	self putAllAt: (Array with: 1@2 with: 1@1 with: 2@2 with: 1@3 with: 3@2).
	
	self assert: (terniLapilli isXWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:37'!
test19XCanWinByFillingTheThirdColumn

	self putAllAt: (Array with: 1@3 with: 1@1 with: 2@3 with: 1@2 with: 3@3).
	
	self assert: (terniLapilli isXWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:40'!
test20XCanWinByFillingTheFirstRow
	
	self putAllAt: (Array with: 1@1 with: 2@1 with: 1@2 with: 2@2 with: 1@3).
	
	self assert: (terniLapilli isXWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:45'!
test21XCanWinByFillingTheSecondRow
	
	self putAllAt: (Array with: 2@1 with: 3@1 with: 2@2 with: 3@2 with: 2@3).
	
	self assert: (terniLapilli isXWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:50'!
test22XCanWinByFillingTheThirdRow
	
	self putAllAt: (Array with: 3@1 with: 2@1 with: 3@2 with: 2@2 with: 3@3).
	
	self assert: (terniLapilli isXWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:55'!
test23XCanWinByFillingTheDownDiagonal
	
	self putAllAt: (Array with: 1@1 with: 2@1 with: 2@2 with: 3@2 with: 3@3).
	
	self assert: (terniLapilli isXWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:15:59'!
test24XCanWinByFillingTheUpDiagonal
	
	self putAllAt: (Array with: 1@3 with: 2@1 with: 2@2 with: 3@2 with: 3@1).
	
	self assert: (terniLapilli isXWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:16:02'!
test25OCanWin
	
	self putAllAt: (Array with: 2@1 with: 1@1 with: 2@2 with: 1@2 with: 3@1 with: 1@3).
	
	self assert: (terniLapilli isOWinner).
	self deny: (terniLapilli isXWinner)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:16:06'!
test26AfterSlidingAXTokenTheFromPositionIsEmptyAndThereIsAXTokenInTheToPosition

	self putAllAt: (Array with: 1@1 with: 1@3 with: 1@2 with: 2@1 with: 3@1 with: 2@2).
	
	terniLapilli slideFrom: 3@1 to: 3@2.
	
	self assert: (terniLapilli isEmptyAt: 3@1).
	self assert: (terniLapilli isXAt: 3@2)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:16:10'!
test27AfterSlidingAOTokenTheFromPositionIsEmptyAndThereIsAOTokenInTheToPosition
	
	self putAllAt: (Array with: 1@1 with: 1@3 with: 1@2 with: 2@1 with: 3@1 with: 2@2).
	
	terniLapilli slideFrom: 3@1 to: 3@2.
	terniLapilli slideFrom: 2@1 to: 3@1.
	
	self assert: (terniLapilli isEmptyAt: 2@1).
	self assert: (terniLapilli isOAt: 3@1)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:16:15'!
test28CanNotSlideATokenFromAPositionOutOfBound
	
	self putAllAt: (Array with: 1@1 with: 1@3 with: 1@2 with: 2@1 with: 3@1 with: 2@2).
	
	self 
		should: [ terniLapilli slideFrom: 4@2 to: 3@2] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli positionOutOfBoundMessageDescription = error messageText.
			self assert: (terniLapilli isEmptyAt: 3@2) ]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:16:19'!
test29CanNotSlideATokenToAPositionOutOfBound
	
	self putAllAt: (Array with: 1@1 with: 1@3 with: 1@2 with: 2@1 with: 3@1 with: 2@2).
	
	self 
		should: [ terniLapilli slideFrom: 3@1 to: 4@1] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli positionOutOfBoundMessageDescription = error messageText.
			self assert: (terniLapilli isXAt: 3@1) ]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/8/2018 12:04:03'!
test30CanNotSlideAXTokenFromAPositionWithoutXToken
	
	self putAllAt: (Array with: 1@1 with: 1@3 with: 1@2 with: 2@1 with: 3@1 with: 2@2).
	
	self 
		should: [ terniLapilli slideFrom: 2@2 to: 3@2] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli canNotSlideFromAPositionIfThePlayerIsNotInThatPositionMessageDescription = error messageText.
			self assert: (terniLapilli isOAt: 2@2).
			self assert: (terniLapilli isEmptyAt: 3@2) ]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/8/2018 12:04:03'!
test31CanNotSlideAOTokenFromAPositionWithoutOToken

	self putAllAt: (Array with: 1@1 with: 1@3 with: 1@2 with: 2@1 with: 3@1 with: 2@2).
	terniLapilli slideFrom: 3@1 to: 3@2. 
	
	self 
		should: [ terniLapilli slideFrom: 2@3 to: 3@3] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli canNotSlideFromAPositionIfThePlayerIsNotInThatPositionMessageDescription = error messageText.
			self assert: (terniLapilli isEmptyAt: 2@3).
			self assert: (terniLapilli isEmptyAt: 3@3) ]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:16:32'!
test32CanNotPutATokenIfXWonByPuttingAToken
	
	self putAllAt: (Array with: 1@1 with: 1@2 with: 2@1 with: 1@3 with:3@1).
	
	self 
		should: [ terniLapilli putAt: 2@2] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli theGameIsOverMessageDescription = error messageText.
			self assert: (terniLapilli isEmptyAt: 2@2).
			self assert: (terniLapilli isXWinner)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:16:35'!
test33CanNotSlideATokenIfOWonByPuttingAToken
	
	self putAllAt: (Array with: 2@1 with: 1@1 with: 2@2 with: 1@2 with: 3@1 with: 1@3).
	
	self 
		should: [ terniLapilli slideFrom: 3@1 to: 3@2]
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli theGameIsOverMessageDescription = error messageText.
			self assert: (terniLapilli isXAt: 3@1).
			self assert: (terniLapilli isEmptyAt: 3@2).
			self assert: (terniLapilli isOWinner)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:16:38'!
test34CanNotSlideATokenIfOWonBySlidingAToken
	
	self putAllAt: (Array with: 1@1 with: 1@3 with: 2@2 with: 2@3 with:3@1 with: 3@2).
	terniLapilli slideFrom: 2@2 to: 1@2.
	terniLapilli slideFrom: 3@2 to: 3@3.
	
	self 
		should: [terniLapilli slideFrom: 1@2 to: 2@2] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli theGameIsOverMessageDescription = error messageText.
			self assert: (terniLapilli isXAt: 1@2).
			self assert: (terniLapilli isEmptyAt: 2@2).
			self assert: (terniLapilli isOWinner)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:17:14'!
test35CanNotSlideATokenIfXWonBySlidingAToken

	self putAllAt: (Array with: 1@1 with: 1@3 with: 2@2 with: 2@3 with:3@1 with: 3@2).
	terniLapilli slideFrom: 2@2 to: 2@1.
	
	self 
		should: [terniLapilli slideFrom: 1@3 to: 1@2] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli theGameIsOverMessageDescription = error messageText.
			self assert: (terniLapilli isEmptyAt: 1@2).
			self assert: (terniLapilli isOAt: 1@3).
			self assert: (terniLapilli isXWinner)]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:17:27'!
test36CanNotSlideMoreThanOneColumn

	self putAllAt: (Array with: 1@1 with: 1@3 with: 1@2 with: 2@1 with: 3@1 with: 2@2). 
	
	self 
		should: [ terniLapilli slideFrom: 3@1 to: 3@3] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli canOnlyMoveToAnAdjacentPositionMessageDescription = error messageText.
			self assert: (terniLapilli isXAt: 3@1).
			self assert: (terniLapilli isEmptyAt: 3@3) ]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:17:30'!
test37CanNotSlideMoreThanOneRow
	
	self putAllAt: (Array with: 1@1 with: 1@3 with: 1@2 with: 2@1 with: 3@1 with: 2@2). 
	terniLapilli slideFrom: 3@1 to: 3@2.
	 
	self 
		should: [ terniLapilli slideFrom: 1@3 to: 3@3] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli canOnlyMoveToAnAdjacentPositionMessageDescription = error messageText.
			self assert: (terniLapilli isOAt: 1@3).
			self assert: (terniLapilli isEmptyAt: 3@3) ]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:17:38'!
test38CanNotSlideInBothDirectionsWithoutPassingThroughTheCenter

	self putAllAt: (Array with: 1@1 with: 1@3 with: 1@2 with: 2@1 with: 3@1 with: 2@2). 

	self 
		should: [ terniLapilli slideFrom: 1@2 to: 2@3] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli canOnlyMoveToAnAdjacentPositionMessageDescription = error messageText.
			self assert: (terniLapilli isXAt: 1@2).
			self assert: (terniLapilli isEmptyAt: 2@3) ]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:17:47'!
test39CanSlideInBothDirectionsFromTheCenter

	self putAllAt: (Array with: 1@1 with: 1@3 with: 2@2 with: 2@3 with:3@1 with: 3@2).
	terniLapilli slideFrom: 2@2 to: 3@3.
	
	self assert: (terniLapilli isXAt: 3@3).
	self assert: (terniLapilli isEmptyAt: 2@2)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:17:50'!
test40CanSlideInBothDirectionsToTheCenter

	self putAllAt: (Array with: 1@1 with: 1@3 with: 3@3 with: 2@3 with:3@1 with: 3@2).
	terniLapilli slideFrom: 3@3 to: 2@2.
	
	self assert: (terniLapilli isXAt: 2@2).
	self assert: (terniLapilli isEmptyAt: 3@3)! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/7/2018 14:17:53'!
test41CanNotSlideATokenWhenNotAllTokensAreInTheBoard
	
	self putAllAt: (Array with: 1@1 with: 1@3).
	
	self 
		should: [ terniLapilli slideFrom: 1@1 to: 1@2] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli notAllTokensInBoardMessageDescription = error messageText.
			self assert: (terniLapilli isXAt: 1@1).
			self assert: (terniLapilli isEmptyAt: 1@2) ]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/6/2018 22:14:51'!
test42CanNotAskIsXAtAPositionOutTheBound
	
	self 
		should: [ terniLapilli isXAt: 4@1 ] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli positionOutOfBoundMessageDescription = error messageText]! !

!TerniLapilliTest methodsFor: 'tests' stamp: 'DEG 11/6/2018 22:15:05'!
test43CanNotAskIsOAtAPositionOutTheBound
	
	self 
		should: [ terniLapilli isOAt: 4@1 ] 
		raise: Error 
		withExceptionDo: [ : error |
			self assert: TerniLapilli positionOutOfBoundMessageDescription = error messageText]! !


!TerniLapilliTest methodsFor: 'initialization' stamp: 'GB 11/6/2018 17:18:03'!
setUp

	terniLapilli := TerniLapilli new.! !


!TerniLapilliTest methodsFor: 'test support' stamp: 'DEG 11/7/2018 14:12:59'!
putAllAt: positions

	positions do: [: aPosition | terniLapilli putAt: aPosition ]! !


!classDefinition: #TerniLapilli category: #TerniLapilli!
Object subclass: #TerniLapilli
	instanceVariableNames: 'oPositions xPositions state'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilli methodsFor: 'playing' stamp: 'GB 11/5/2018 21:49:07'!
putAt: aPosition
	
	self checkPositionInBound: aPosition.
	( self isEmptyAt: aPosition ) ifFalse: [self error: self class notEmptyPositionMessageDescription].
	
	state putAt: aPosition 	! !

!TerniLapilli methodsFor: 'playing' stamp: 'GB 11/5/2018 21:50:16'!
slideFrom: fromPosition to: toPosition

	self checkPositionInBound: fromPosition.
	self checkPositionInBound: toPosition.
	(self isEmptyAt: toPosition) ifFalse: [self error: self class notEmptyPositionMessageDescription].
	self checkThat: fromPosition isAdjacentTo: toPosition.
	
	state slideFrom: fromPosition to: toPosition 
	
	
		
	
	! !


!TerniLapilli methodsFor: 'testing - private' stamp: 'DEG 11/7/2018 14:06:28'!
hasACompleteColumn: tokens
	
	^ (1 to: 3) anySatisfy: [: col | (1 to: 3) allSatisfy: [: row | tokens includes: row@col] ]! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'DEG 11/7/2018 14:06:34'!
hasACompleteDownDiagonal: tokens
	
	^ (1 to: 3) allSatisfy: [: row | tokens includes: row@row]! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'DEG 11/7/2018 14:06:39'!
hasACompleteRow: tokens
	
	^ (1 to: 3) anySatisfy: [: row | (1 to: 3) allSatisfy: [: col | tokens includes: row@col] ]! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'DEG 11/7/2018 14:06:45'!
hasACompleteUpDiagonal: tokens
	
	^ (1 to: 3) allSatisfy: [: row | tokens includes: row@(4-row)]! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'DEG 11/7/2018 14:06:48'!
hasALineOfThree: tokens
	
	^ (self hasACompleteColumn: tokens) or:
	   [self hasACompleteRow: tokens] or:
	   [self hasACompleteDownDiagonal: tokens] or:
	   [self hasACompleteUpDiagonal: tokens]! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:34:12'!
whenPuttingOIsOWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:34:17'!
whenPuttingOIsXWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:34:08'!
whenPuttingXIsOWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:33:57'!
whenPuttingXIsXWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:46:22'!
whenSlidingOIsOWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:46:29'!
whenSlidingOIsXWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:47:50'!
whenSlidingXIsOWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:47:44'!
whenSlidingXIsXWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:41:16'!
whenWinnerOIsOWinner

	^ true! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:41:28'!
whenWinnerOIsXWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:39:49'!
whenWinnerXIsOWinner

	^ false! !

!TerniLapilli methodsFor: 'testing - private' stamp: 'GB 11/5/2018 21:40:04'!
whenWinnerXIsXWinner

	^ true! !


!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:54:28'!
whenPuttingOPutAt: aPosition

	oPositions add: aPosition.
	
	( self hasALineOfThree: oPositions ) 
		ifTrue: [state := WinnerOState for: self]
		ifFalse: [
			(oPositions size = 3) 
				ifTrue: [state := SlidingXState for: self]
				ifFalse: [state := PuttingXState for: self]
				]
		! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:36:40'!
whenPuttingOSlideFrom: fromPosition to: toPosition

	^ self error: self class notAllTokensInBoardMessageDescription ! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:52:43'!
whenPuttingXPutAt: aPosition

	xPositions add: aPosition. 
	
	( self hasALineOfThree: xPositions ) 
		ifTrue: [state := WinnerXState for: self]
		ifFalse: [state := PuttingOState for: self]! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:35:48'!
whenPuttingXSlideFrom: fromPosition to: toPosition

	^ self error: self class notAllTokensInBoardMessageDescription ! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:46:07'!
whenSlidingOPutAt: aPosition

	self error: self class allTokensInBoardMessageDescription ! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'DEG 11/8/2018 12:04:03'!
whenSlidingOSlideFrom: fromPosition to: toPosition

	oPositions remove: fromPosition ifAbsent: [self error: self class canNotSlideFromAPositionIfThePlayerIsNotInThatPositionMessageDescription].
	oPositions add: toPosition. 
	
	(self hasALineOfThree: oPositions) 
		ifTrue: [state := WinnerOState for: self]
		ifFalse: [state := SlidingXState for: self]! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:47:38'!
whenSlidingXPutAt: aPosition

	self error: self class allTokensInBoardMessageDescription ! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'DEG 11/8/2018 12:04:03'!
whenSlidingXSlideFrom: fromPosition to: toPosition

	xPositions remove: fromPosition ifAbsent: [self error: self class canNotSlideFromAPositionIfThePlayerIsNotInThatPositionMessageDescription].
	xPositions add: toPosition. 
	
	(self hasALineOfThree: xPositions) 
		ifTrue: [state := WinnerXState for: self]
		ifFalse: [state := SlidingOState for: self]! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:41:04'!
whenWinnerOPutAt: aPosition

	self error: self class theGameIsOverMessageDescription ! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:40:55'!
whenWinnerOSlideFrom: fromPosition to: toPosition

	^ self error: self class theGameIsOverMessageDescription ! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:39:15'!
whenWinnerXPutAt: aPosition

	self error: self class theGameIsOverMessageDescription ! !

!TerniLapilli methodsFor: 'playing - private' stamp: 'GB 11/5/2018 21:38:52'!
whenWinnerXSlideFrom: fromPosition to: toPosition

	^ self error: self class theGameIsOverMessageDescription ! !


!TerniLapilli methodsFor: 'testing' stamp: 'GB 11/5/2018 18:21:51'!
isEmpty

	^ xPositions isEmpty! !

!TerniLapilli methodsFor: 'testing' stamp: 'GB 11/5/2018 18:21:51'!
isEmptyAt: aPosition
	
	self checkPositionInBound: aPosition.
	^ ( (xPositions includes: aPosition ) or: [oPositions includes: aPosition ] ) not! !

!TerniLapilli methodsFor: 'testing' stamp: 'DEG 11/6/2018 22:15:53'!
isOAt: aPosition
	
	self checkPositionInBound: aPosition.
	^ oPositions includes: aPosition ! !

!TerniLapilli methodsFor: 'testing' stamp: 'GB 11/5/2018 21:50:49'!
isOWinner
	
	^ state isOWinner ! !

!TerniLapilli methodsFor: 'testing' stamp: 'DEG 11/6/2018 22:16:06'!
isXAt: aPosition

	self checkPositionInBound: aPosition.
	^ xPositions includes: aPosition ! !

!TerniLapilli methodsFor: 'testing' stamp: 'GB 11/5/2018 21:50:58'!
isXWinner
	
	^ state isXWinner 
	! !


!TerniLapilli methodsFor: 'initialization' stamp: 'GB 11/5/2018 21:48:21'!
initialize
	
	xPositions := OrderedCollection new.
	oPositions := OrderedCollection new.
	state := PuttingXState for: self! !


!TerniLapilli methodsFor: 'assertions' stamp: 'DEG 11/7/2018 14:10:10'!
adjacentPositionsOf: aPosition

	((2@2) fourNeighbors includes: aPosition ) ifTrue: [^aPosition fourNeighbors].
	^aPosition eightNeighbors ! !

!TerniLapilli methodsFor: 'assertions' stamp: 'GB 11/5/2018 17:08:34'!
checkPositionInBound: aPosition

	(aPosition between: 1@1 and: 3@3) ifFalse: [self error: self class positionOutOfBoundMessageDescription].! !

!TerniLapilli methodsFor: 'assertions' stamp: 'DEG 11/7/2018 14:10:47'!
checkThat: fromPosition isAdjacentTo: toPosition

	((self adjacentPositionsOf: fromPosition) includes: toPosition)
		ifFalse: [self error: self class canOnlyMoveToAnAdjacentPositionMessageDescription]
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TerniLapilli class' category: #TerniLapilli!
TerniLapilli class
	instanceVariableNames: ''!

!TerniLapilli class methodsFor: 'error messages' stamp: 'GB 11/5/2018 17:20:03'!
allTokensInBoardMessageDescription

	^ 'All tokens in board'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'DEG 11/8/2018 12:04:51'!
canNotSlideFromAPositionIfThePlayerIsNotInThatPositionMessageDescription

	^ 'Can not slide from a position if the player is not in that position'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'GB 11/5/2018 19:57:30'!
canOnlyMoveToAnAdjacentPositionMessageDescription

	^ 'Can only move to an adjacent position'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'GB 11/5/2018 21:23:45'!
notAllTokensInBoardMessageDescription

	^ 'Not all tokens in board'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'GB 11/1/2018 20:54:36'!
notEmptyPositionMessageDescription

	^ 'Not empty position'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'GB 11/1/2018 19:50:30'!
positionOutOfBoundMessageDescription
	
	^ 'Position out of bound'! !

!TerniLapilli class methodsFor: 'error messages' stamp: 'GB 11/5/2018 19:44:25'!
theGameIsOverMessageDescription

	^ 'The game is over'! !


!classDefinition: #TerniLapilliState category: #TerniLapilli!
Object subclass: #TerniLapilliState
	instanceVariableNames: 'game'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!TerniLapilliState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:26:23'!
putAt: aPosition

	self subclassResponsibility ! !

!TerniLapilliState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:26:39'!
slideFrom: fromPosition to: toPosition

	self subclassResponsibility ! !


!TerniLapilliState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:26:51'!
isOWinner

	self subclassResponsibility ! !

!TerniLapilliState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:26:49'!
isXWinner

	self subclassResponsibility ! !


!TerniLapilliState methodsFor: 'initialization' stamp: 'GB 11/5/2018 21:26:09'!
initializeFor: aGame

	game := aGame ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TerniLapilliState class' category: #TerniLapilli!
TerniLapilliState class
	instanceVariableNames: ''!

!TerniLapilliState class methodsFor: 'instance creation' stamp: 'GB 11/5/2018 21:25:47'!
for: aGame

	^ self new initializeFor: aGame! !


!classDefinition: #PuttingOState category: #TerniLapilli!
TerniLapilliState subclass: #PuttingOState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!PuttingOState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:36:22'!
putAt: aPosition

	game whenPuttingOPutAt: aPosition ! !

!PuttingOState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:36:28'!
slideFrom: fromPosition to: toPosition

	game whenPuttingOSlideFrom: fromPosition to: toPosition ! !


!PuttingOState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:36:13'!
isOWinner

	^ game whenPuttingOIsOWinner! !

!PuttingOState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:36:17'!
isXWinner

	^ game whenPuttingOIsXWinner! !


!classDefinition: #PuttingXState category: #TerniLapilli!
TerniLapilliState subclass: #PuttingXState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!PuttingXState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:29:31'!
putAt: aPosition

	game whenPuttingXPutAt: aPosition ! !

!PuttingXState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:35:23'!
slideFrom: fromPosition to: toPosition

	game whenPuttingXSlideFrom: fromPosition to: toPosition ! !


!PuttingXState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:34:36'!
isOWinner

	^ game whenPuttingXIsOWinner! !

!PuttingXState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:33:39'!
isXWinner

	^ game whenPuttingXIsXWinner! !


!classDefinition: #SlidingOState category: #TerniLapilli!
TerniLapilliState subclass: #SlidingOState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!SlidingOState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:42:11'!
putAt: aPosition

	game whenSlidingOPutAt: aPosition ! !

!SlidingOState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:42:20'!
slideFrom: fromPosition to: toPosition

	game whenSlidingOSlideFrom: fromPosition to: toPosition ! !


!SlidingOState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:41:53'!
isOWinner

	^ game whenSlidingOIsOWinner! !

!SlidingOState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:42:03'!
isXWinner

	^ game whenSlidingOIsXWinner! !


!classDefinition: #SlidingXState category: #TerniLapilli!
TerniLapilliState subclass: #SlidingXState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!SlidingXState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:46:52'!
isOWinner

	^ game whenSlidingXIsOWinner! !

!SlidingXState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:46:58'!
isXWinner

	^ game whenSlidingXIsXWinner! !


!SlidingXState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:47:03'!
putAt: aPosition

	game whenSlidingXPutAt: aPosition ! !

!SlidingXState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:47:09'!
slideFrom: fromPosition to: toPosition

	game whenSlidingXSlideFrom: fromPosition to: toPosition ! !


!classDefinition: #WinnerOState category: #TerniLapilli!
TerniLapilliState subclass: #WinnerOState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!WinnerOState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:40:20'!
isOWinner

	^ game whenWinnerOIsOWinner! !

!WinnerOState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:40:30'!
isXWinner

	^ game whenWinnerOIsXWinner! !


!WinnerOState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:40:37'!
putAt: aPosition

	game whenWinnerOPutAt: aPosition ! !

!WinnerOState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:40:44'!
slideFrom: fromPosition to: toPosition

	game whenWinnerOSlideFrom: fromPosition to: toPosition ! !


!classDefinition: #WinnerXState category: #TerniLapilli!
TerniLapilliState subclass: #WinnerXState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TerniLapilli'!

!WinnerXState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:37:52'!
isOWinner

	^ game whenWinnerXIsOWinner! !

!WinnerXState methodsFor: 'testing' stamp: 'GB 11/5/2018 21:38:04'!
isXWinner

	^ game whenWinnerXIsXWinner! !


!WinnerXState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:38:13'!
putAt: aPosition

	game whenWinnerXPutAt: aPosition ! !

!WinnerXState methodsFor: 'playing' stamp: 'GB 11/5/2018 21:38:21'!
slideFrom: fromPosition to: toPosition

	game whenWinnerXSlideFrom: fromPosition to: toPosition ! !
