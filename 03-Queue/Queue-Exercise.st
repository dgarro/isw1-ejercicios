!classDefinition: #QueueTest category: #'Queue-Exercise'!
TestCase subclass: #QueueTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test01QueueShouldBeEmptyWhenCreated

	| queue |

	queue _ Queue new.

	self assert: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test02EnqueueAddElementsToTheQueue

	| queue |

	queue _ Queue new.
	queue enqueue: 'Something'.

	self deny: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test03DequeueRemovesElementsFromTheQueue

	| queue |

	queue _ Queue new.
	queue enqueue: 'Something'.
	queue dequeue.

	self assert: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test04DequeueReturnsFirstEnqueuedObject

	| queue firstQueued secondQueued |

	queue _ Queue new.
	firstQueued _ 'Something1'.
	secondQueued _ 'Something2'.
	queue enqueue: firstQueued.
	queue enqueue: secondQueued.
	
	self assert: queue dequeue equals: firstQueued.! !

!QueueTest methodsFor: 'test' stamp: 'CGCM 9/27/2018 16:34:58'!
test05QueueBehavesFIFO

	| queue firstQueued secondQueued |

	queue _ Queue new.
	firstQueued _ 'First'.
	secondQueued _ 'Second'.
	queue enqueue: firstQueued.
	queue enqueue: secondQueued.

	self assert: queue dequeue equals: firstQueued.
	self assert: queue dequeue equals: secondQueued.
	self assert: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test06NextReturnsFirstEnqueuedObject

	| queue firstQueued |

	queue _ Queue new.
	firstQueued _ 'Something'.
	queue enqueue: firstQueued.

	self assert: queue next equals: firstQueued.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test07NextDoesNotRemoveObjectFromQueue

	| queue firstQueued |

	queue _ Queue new.
	firstQueued _ 'Something'.
	queue enqueue: firstQueued.

	queue next.
	self assert: queue size equals: 1.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test08CanNotDequeueWhenThereAreNoObjectsInTheQueue

	| queue |

	queue _ Queue new.
	
	self
		should: [ queue dequeue ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Queue queueEmptyErrorDescription ].! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test09CanNotDequeueWhenThereAreNoObjectsInTheQueueAndTheQueueHadObjects

	| queue |

	queue _ Queue new.
	queue enqueue: 'Something'.
	queue dequeue.

	self
		should: [ queue dequeue ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Queue queueEmptyErrorDescription ].! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test10CanNotNextWhenThereAreNoObjectsInTheQueue

	| queue |

	queue _ Queue new.

	self
		should: [ queue next ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Queue queueEmptyErrorDescription ].! !


!classDefinition: #NextElementQueueOperationsHandler category: #'Queue-Exercise'!
Object subclass: #NextElementQueueOperationsHandler
	instanceVariableNames: 'queue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!NextElementQueueOperationsHandler methodsFor: 'modifying' stamp: 'GB 10/3/2018 16:31:25'!
dequeue

	self subclassResponsibility ! !


!NextElementQueueOperationsHandler methodsFor: 'accessing' stamp: 'GB 10/3/2018 16:31:01'!
next

	self subclassResponsibility ! !


!NextElementQueueOperationsHandler methodsFor: 'initialization' stamp: 'GB 10/3/2018 16:30:27'!
initializeFor: aQueue

	queue := aQueue ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NextElementQueueOperationsHandler class' category: #'Queue-Exercise'!
NextElementQueueOperationsHandler class
	instanceVariableNames: ''!

!NextElementQueueOperationsHandler class methodsFor: 'testing' stamp: 'GB 10/3/2018 16:25:44'!
isFor: aQueue

	self subclassResponsibility ! !


!NextElementQueueOperationsHandler class methodsFor: 'instance creation' stamp: 'GB 10/3/2018 16:49:46'!
for: aQueue

	^ self new initializeFor: aQueue! !


!NextElementQueueOperationsHandler class methodsFor: 'dequeuer search' stamp: 'GB 10/4/2018 16:16:32'!
nextElementQueueOperationsHandlerFor: aQueue

	|nextElementQueueOperationsHandlerSubclass|
	
	nextElementQueueOperationsHandlerSubclass := 
	self subclasses detect: [:aNextElementQueueOperationsHandlerSubclass | aNextElementQueueOperationsHandlerSubclass isFor: aQueue ].
	
	^ nextElementQueueOperationsHandlerSubclass for: aQueue
	
	! !


!classDefinition: #NextElementEmptyQueueOperationsHandler category: #'Queue-Exercise'!
NextElementQueueOperationsHandler subclass: #NextElementEmptyQueueOperationsHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!NextElementEmptyQueueOperationsHandler methodsFor: 'modifying' stamp: 'GB 10/3/2018 17:21:12'!
dequeue

	queue signalQueueEmptyError ! !


!NextElementEmptyQueueOperationsHandler methodsFor: 'accessing' stamp: 'GB 10/3/2018 17:20:52'!
next

	queue signalQueueEmptyError ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NextElementEmptyQueueOperationsHandler class' category: #'Queue-Exercise'!
NextElementEmptyQueueOperationsHandler class
	instanceVariableNames: ''!

!NextElementEmptyQueueOperationsHandler class methodsFor: 'testing' stamp: 'GB 10/3/2018 16:26:00'!
isFor: aQueue

	^ aQueue isEmpty ! !


!classDefinition: #NextElementNonEmptyQueueOperationsHandler category: #'Queue-Exercise'!
NextElementQueueOperationsHandler subclass: #NextElementNonEmptyQueueOperationsHandler
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!NextElementNonEmptyQueueOperationsHandler methodsFor: 'modifying' stamp: 'GB 10/3/2018 16:43:48'!
dequeue

	^ queue safeDequeue ! !


!NextElementNonEmptyQueueOperationsHandler methodsFor: 'accessing' stamp: 'GB 10/3/2018 16:43:36'!
next

	^ queue safeNext ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NextElementNonEmptyQueueOperationsHandler class' category: #'Queue-Exercise'!
NextElementNonEmptyQueueOperationsHandler class
	instanceVariableNames: ''!

!NextElementNonEmptyQueueOperationsHandler class methodsFor: 'testing' stamp: 'GB 10/3/2018 16:26:22'!
isFor: aQueue

	^ aQueue isEmpty not! !


!classDefinition: #Queue category: #'Queue-Exercise'!
Object subclass: #Queue
	instanceVariableNames: 'contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!Queue methodsFor: 'modifying' stamp: 'GB 10/4/2018 16:17:30'!
dequeue
	
	^ (NextElementQueueOperationsHandler nextElementQueueOperationsHandlerFor: self) dequeue 
	
! !

!Queue methodsFor: 'modifying' stamp: 'GB 10/1/2018 21:40:17'!
enqueue: anElementToEnqueue 
	
	contents addLast: anElementToEnqueue ! !


!Queue methodsFor: 'modifying - private' stamp: 'GB 10/3/2018 16:39:48'!
safeDequeue

	^ contents removeFirst ! !


!Queue methodsFor: 'testing' stamp: 'GB 10/1/2018 21:31:53'!
isEmpty

	^ contents isEmpty ! !


!Queue methodsFor: 'accessing' stamp: 'GB 10/4/2018 16:17:44'!
next
	
	^ (NextElementQueueOperationsHandler nextElementQueueOperationsHandlerFor: self) next 
! !

!Queue methodsFor: 'accessing' stamp: 'GB 10/1/2018 21:46:49'!
size

	^ contents size! !


!Queue methodsFor: 'accessing - private' stamp: 'GB 10/3/2018 16:39:02'!
safeNext

	^ contents first ! !


!Queue methodsFor: 'initialization' stamp: 'GB 10/1/2018 21:37:53'!
initialize

	contents := OrderedCollection new! !


!Queue methodsFor: 'error signaling' stamp: 'GB 10/3/2018 17:20:24'!
signalQueueEmptyError

	self error: self class queueEmptyErrorDescription ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Queue class' category: #'Queue-Exercise'!
Queue class
	instanceVariableNames: ''!

!Queue class methodsFor: 'error descriptions'!
queueEmptyErrorDescription
	^ 'Queue is empty'.! !
